---
layout: layout.html
---

# 6 de febrero de 2020 00:19

Hoy ha sido productivo, he conseguido:

- Añadir el menú principal (muy básico de momento) y que sea funcional.
- Añadir un texto de introducción al nivel.
- Empezar un nuevo tipo de arma.
- Bocetar cómo se crearán los niveles.
- Probar con GameJolt y la API para ganar trofeos, logros y demás.
- Probar cambios en los gráficos de las naves.
- Añadir fuentes.

Sobre el sistema de creación de niveles, ésto es lo que tengo pensado: un nivel se
compondrá siempre de un .FPG que contendrá los fondos, el plano principal será de
320x8000 (o algo así) y el plano secundario deberá ser cíclico y de 320x400.

Por otra parte habrá un archivo .DAT que contendrá una lista de comandos, los comandos
serán algo así como _instrucciones_ para el nivel. Estas instrucciones servirán
principalmente para: generar oleadas de enemigos, generar enemigos finales, generar
items, mostrar texto o reproducir sonido y poco más.

```
struct commands[1023]
  int time, kind, params[8];
end
```

De esta forma lo que haré es tener un _timer_ y un índice reservado para el control de
los comandos y en el bucle del proceso _level_ haré algo de este estilo:

```
while (commands[command_index].time <= timer[TIMER_LEVEL])
  // Todavía no tengo claro si utilizar esto.
  command_delta = timer[TIMER_LEVEL] - commands[command_index].time
  // Ejecutamos el comando
  switch (commands[command_index].kind)
    case 0: // Generar oleada enemiga.
      // ...
    end
    case 1: // Reproducir sonido.
      // ...
    end
    case 2: // Mostrar mensaje.
      // ...
    end
  end

  // Incrementamos el comando en el que nos encontramos.
  command_index++;

  // Si llegamos al final de la lista deberíamos salir.
  // No tengo muy claro si esto debería dar un error y salir
  // con exit("Error nivel", 1) o algo así.
  if (command_index == 1024)
    break;
  end
end
```

El juego lo primero que hará es cargar un archivo con la lista
de niveles y después cargará todos los recursos que estén compartidos
en el juego: sonidos de menú, música, fondos, etc.

Y cuando arranque el juego, independientemente del modo, cargará el primer
nivel empezando por el índice 0 de la lista de niveles que cargamos al
principio y el jugador irá pasando niveles hasta llegar al final, cuándo
llegue al final irá a créditos y de ahí al menú principal.

# 5 de febrero de 2020 00:06

Bueno, hoy me ha dado tiempo a muy poco.

Por una parte me he bajado MilkyTracker (una adaptación moderna y Open Source del
mítico Fast Tracker 2). También me he bajado un montón de _samples_ e instrumentos
para poder usarlos en Milky Tracker.

Y por otra parte he estado escuchando canciones en [https://modarchive.org/](https://modarchive.org/)
que pudiesen pegar con el juego.

![Milky Tracker reproduciendo DARKSHIP.XM](/images/milky.png)

# 3 de febrero de 2020 00:03

Hoy no he tenido prácticamente tiempo para continuar el juego así que he
hecho pequeños avances.

Este es el último cambio que hago entre texto e imágenes. Todos los procesos
ya tienen sus correspondientes gráficos. He borrado el dibujado de curvas aunque
creo que podría ser útil para mostrar las animaciones de los enemigos.

<iframe width="560" height="315" src="https://www.youtube.com/embed/5BRsOiKXYPs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# 2 de febrero de 2020 11:23

He cambiado la manera en la que se cargan los recursos (antes eran todo .map
individuales y además cargaba de nuevo cada vez que se creaba un proceso).

Y además he probado el control con un jugador y con dos jugadores.

<iframe width="560" height="315" src="https://www.youtube.com/embed/wpvz3_nBqgk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# 2 de febrero de 2020 03:35

Mañana no creo que pueda hacer absolutamente nada así que aprovecho ahora
para meter gráficos y hacer algunas pruebas con el scroll.

Al principio pensé en utilizar _define\_region()_ y _out\_region()_ para
implementar un sistema de cámara que siguiese a la nave cuando se moviese
el scroll pero no tenía mucho sentido.

Después pensé en utiliza _scroll.camera_ pero no me termina de gustar cómo
está implementado el sistema de cámaras de scroll.

Así que finalmente he implementado mi propio sistema. Básicamente lo que
hace es comprobar si el scroll está en sus límites y si no está en sus
límites, cuando la nave supera ciertas coordenadas en x mueve el scroll
tanto como la velocidad de la nave.

```
if (x < 100 and scroll.x0 > 0)
  x = 100;
  scroll.x0 += vel_x;
end

if (x > 220 and scroll.x0 < 128)
  x = 100;
  scroll.x0 += vel_x;
end

if (x < 10)
  x = 10;
end

if (x > 310)
  x = 310;
end
```

<iframe width="560" height="315" src="https://www.youtube.com/embed/AP5LeD5DSF0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Otra cosa que he hecho es aprovechar para jugar un rato al Raptor: Call
of the Shadows, al Tyrian y al Xenon 2: MegaBlast. Tres de los grandes
Shoot'em ups para PC.

Como curiosidad: la paleta que estoy usando ahora mismo es la del
Raptor: Call of the Shadows.

# 1 de febrero de 2020 10:58

He podido sacar un ratito esta mañana y he aprovechado para implementar
los disparos laterales, diagonales y misiles. Como estoy utilizando las
funciones de texto para el prototipado, de momento los caracteres que he
usado son: A para la nave del jugador, s para los _sidekicks_, o para un
orbe que da vueltas alrededor de la nave, | para los misiles, i para los
disparos y V para los enemigos.

<iframe width="560" height="315" src="https://www.youtube.com/embed/CJfBOASNB3w" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# 1 de febrero de 2020 01:22

La DIV Compo arrancó ¡por fin! Ya tenía ganas de poder empezar a picar
código y a darle vueltas al coco. En el repo ya he subido una primera
prueba utilizando texto como sprites de las naves para no tener que
andar perdiendo demasiado tiempo con el editor de mapas.

He intentado grabar toda la sesión con DOSBox (si presionas CTRL+SHIFT+F5
arranca y detiene la grabación de vídeo) pero por algún extraño motivo
DOSBox no me ha guardado los archivos (normalmente lo hace en la carpeta
_capture_).

Pero vamos a lo más importante ¿qué voy a hacer? En principio tengo dos
ideas totalmente opuestas XD. Por una parte tengo ganas de hacer algo
diferente y original, una especie de matamarcianos en el que llevas una
moto y el juego son peleas de motos al estilo de Akira o Full Throttle.

Y por otra parte me gustaria hacer un matamarcianos canónico, con sus
armas de rayos, sus misiles, sus _bosses_ gigantes y sus enemigos
deterministas. De momento va ganando esta idea porque creo que va a
ser la más fácil de hacer.

Mañana si tengo tiempo dibujaré algunas navecitas y a lo mejor algún
item o algo así y de paso aterrizaré un poco las ideas.

