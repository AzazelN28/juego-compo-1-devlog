# Devlog de la DIV Compo 2020

Creado con [metalsmith](https://metalsmith.io) se puede publicar automáticamente en desarrollo con:

```sh
now
```

y en producción con:

```sh
now --prod
```

Para el repositorio del juego consultar https://gitlab.com/azazeln28/juego-compo-1

Made with :heart: by [AzazelN28](https://github.com/AzazelN28)
